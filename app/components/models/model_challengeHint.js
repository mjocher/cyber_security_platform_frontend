angular.
module('cscFrontend.model_challengeHint', ['ngResource']).
factory('ChallengeHint', function(config, $resource) {
        return $resource(config.apiUrl + '/challenges/:challengeId/hints/:hintId', {}, {

        });
    }
);