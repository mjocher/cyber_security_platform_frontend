'use strict';

angular.module('cscFrontend.service_notification', [])
    .factory('NotificationService', function($rootScope) {
        $rootScope.$on('$routeChangeStart', function(angularEvent, newUrl) {
            $rootScope.notification = null;
        });
        return {
            success: function(message) {
                $rootScope.notification = {
                    message: message,
                    type: "success"
                }
            },
            danger: function(message) {
                $rootScope.notification = {
                    message: message,
                    type: "danger"
                }
            },
            warning: function(message) {
                $rootScope.notification = {
                    message: message,
                    type: "warning"
                }
            },
            info: function(message) {
                $rootScope.notification = {
                    message: message,
                    type: "info"
                }
            },
            reset: function() {
                $rootScope.notification = null;
            }
        };
    });