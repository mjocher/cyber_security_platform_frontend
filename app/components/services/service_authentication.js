'use strict';

angular.module('cscFrontend.service_authentication', ['cscFrontend.service_session', 'cscFrontend.model_user'])
    .factory('AuthenticationService', function(config, $location, $http, SessionService, User) {
        return {
            login: function(username, password, callback) {
                return $http.post(config.apiUrl + '/auth/credentials/', { UserName: username, Password: password })
                    .then(function(response) {
                        User.authenticatedUser().$promise.then(function(u) {
                            SessionService.setUser(u);
                            $location.path("/view_main");
                        });
                    });
            },
            logout: function() {
                return $http.post(config.apiUrl + '/auth/logout/')
                    .then(function(response) {
                        SessionService.destroy();
                        $location.path("/view_welcome");
                    });
            },
            isLoggedIn: function() {
                return SessionService.getUser() != null;
            }
        };
    });